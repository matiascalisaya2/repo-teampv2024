package ar.edu.unju.fi.tp6.service;

import java.util.List;

import ar.edu.unju.fi.tp6.model.Compra;


public interface ICompraService {
	public List<Compra> listarCompras();
	public void agregarCompra(Compra compra);
	public Compra buscarCompra(int id) ;
	public void eliminarCompra(Compra compra);

}
