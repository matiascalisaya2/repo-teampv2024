package ar.edu.unju.fi.tp6.service.imp;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import ar.edu.unju.fi.tp6.model.Producto;
import ar.edu.unju.fi.tp6.service.IProductoService;



@Service
public class ServiceProductoImp implements IProductoService {
	List<Producto> listaProtuctos=new ArrayList<>();
	
	@Override
	public void agregarProducto(Producto producto) {
			Producto produ= buscarProducto(producto.getCodigo());
		if (produ==null) {
			listaProtuctos.add(producto);
		}else {
			int pos=listaProtuctos.indexOf(produ);
			listaProtuctos.set(pos, producto);
		}
		
	}

	@Override
	public List<Producto> listaProductos() {
		
		return listaProtuctos;
	}

	@Override
	public Producto ultimoproducto() {
		Producto ultimoProducto = listaProtuctos.get(listaProtuctos.size()-1);
		return ultimoProducto;
	}

	@Override
	public Producto buscarProducto(int codigo) {
		Producto productoBuscado=null;
		
		for (Producto produc : listaProtuctos) {
			if (produc.getCodigo()==codigo) {
				productoBuscado=produc;
				break;
			}
		}
		return productoBuscado; 
	}

	@Override
	public void eliminarProducto(Producto prod) {
		listaProtuctos.remove(prod);
		
	}

	@Override
	public String mostrarNombreProducto(int codigo) {
		// TODO Auto-generated method stub
		Producto productoBuscado = null;

		for (Producto produc : listaProtuctos) {
			if (produc.getCodigo() == codigo) {
				productoBuscado = produc;
				break;
			}
		}
		return productoBuscado.getNombre();
	}

	@Override
	public String mostrarPrecioProducto(int codigo) {
		// TODO Auto-generated method stub
		Producto productoBuscado = null;

		for (Producto produc : listaProtuctos) {
			if (produc.getCodigo() == codigo) {
				productoBuscado = produc;
				break;
			}
		}
		return " "+productoBuscado.getPrecio();
	}

	@Override
	public String mostrarMarcaProducto(int codigo) {
		// TODO Auto-generated method stub
		Producto productoBuscado = null;

		for (Producto produc : listaProtuctos) {
			if (produc.getCodigo() == codigo) {
				productoBuscado = produc;
				break;
			}
		}
		return productoBuscado.getMarca();
	}
	
	

}
