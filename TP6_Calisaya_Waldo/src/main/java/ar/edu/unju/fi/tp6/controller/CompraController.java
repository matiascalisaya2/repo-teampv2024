package ar.edu.unju.fi.tp6.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import ar.edu.unju.fi.tp6.model.Compra;
import ar.edu.unju.fi.tp6.service.ICompraService;
import ar.edu.unju.fi.tp6.service.IProductoService;



@Controller
public class CompraController {
	
	@Autowired
	ICompraService serviceCompra;

	@Autowired
	IProductoService serviceProducto;

	@GetMapping("/addcompra")
	public String getNewCompra(Model model) {
		Compra compra = new Compra();
		model.addAttribute("titulo", "Agregar Compra");
		model.addAttribute("compraNuevo", compra);
		model.addAttribute("listaproductos", serviceProducto.listaProductos());
		return "addCompra";
	}

	@PostMapping("/guardarcompra")
	public String guardar(Compra compraNuevo) {
		serviceCompra.agregarCompra(compraNuevo);
		return "redirect:/listarcompra";
	}

	@GetMapping("/listarcompra")
	public String getMethodName(Model model) {
		List<Compra> compras = serviceCompra.listarCompras();
		model.addAttribute("compras", compras);
		model.addAttribute("contCompra", serviceCompra.listarCompras().size());
		model.addAttribute("serviceproducto", serviceProducto);
		return "crudCompra";
	}

//********* EDITAR ******************
	@GetMapping("/editarCompra/{id}")
	public String editarCompra(@PathVariable("id") int id, Model modelo) {

		Compra compraEditar = serviceCompra.buscarCompra(id);
		modelo.addAttribute("titulo", "Editar Compra");
		modelo.addAttribute("compraNuevo", compraEditar);
		modelo.addAttribute("listaproductos", serviceProducto.listaProductos());
		return "addcompra";
	}

//********* ELIMINAR ******************
	@GetMapping("/deletCompra/{id}")
	public String eliminarCompra(@PathVariable("id") int id, Model modelo) {

		Compra compraEliminar = serviceCompra.buscarCompra(id);
		serviceCompra.eliminarCompra(compraEliminar);

		return "redirect:/listarcompra";
	}
}
