package ar.edu.unju.fi.tp6.service;

import java.util.List;


import ar.edu.unju.fi.tp6.model.Producto;



public interface IProductoService {

	public void agregarProducto(Producto producto);
	
	public List<Producto> listaProductos();
	
	public Producto ultimoproducto();
	
	public Producto buscarProducto(int codigo);
	
	public void eliminarProducto(Producto prod);
	
	public String mostrarNombreProducto(int codigo);
	public String mostrarPrecioProducto(int codigo);
	public String mostrarMarcaProducto(int codigo);
}
