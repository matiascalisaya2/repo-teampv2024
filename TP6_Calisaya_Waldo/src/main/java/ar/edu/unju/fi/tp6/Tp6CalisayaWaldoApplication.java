package ar.edu.unju.fi.tp6;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Tp6CalisayaWaldoApplication {

	public static void main(String[] args) {
		SpringApplication.run(Tp6CalisayaWaldoApplication.class, args);
		
		/*
		 * GRUPO: repo-teamPV2024
		 * INTEGRANTES:
		 * 				Judith Aylen Ruth Colqui
		 * 				Noelia Raquel Ruth Acho
		 * 				Anabella Genesis Acho
		 * 				Matias Waldo Calisaya
		 * 
		 */
	}

}
