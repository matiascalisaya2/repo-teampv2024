package ar.edu.unju.fi.tp6.model;

import org.springframework.stereotype.Component;

@Component
public class Compra {
	private int id;
	private Producto producto;
	private int cantidad;
	
	//Contructores 
	
	public Compra(int id, Producto producto, int cantidad) {
		super();
		this.id = id;
		this.producto = producto;
		this.cantidad = cantidad;
	}
	public Compra() {
		super();
	}
	
	//getters y setters
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Producto getProducto() {
		return producto;
	}
	public void setProducto(Producto producto) {
		this.producto = producto;
	}
	public int getCantidad() {
		return cantidad;
	}
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	
	

}
