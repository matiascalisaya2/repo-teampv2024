package ar.edu.unju.fi.tp6.service.imp;


import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.edu.unju.fi.tp6.model.Compra;
import ar.edu.unju.fi.tp6.model.Producto;
import ar.edu.unju.fi.tp6.service.ICompraService;
import ar.edu.unju.fi.tp6.service.IProductoService;

@Service
public class ServiceCompraImp implements ICompraService{

	List<Compra> listaCompra = new ArrayList<>();
	@Autowired
    private IProductoService productoService;
	
	@Override
	public List<Compra> listarCompras() {
	
		return listaCompra;
	}

	@Override
	public void agregarCompra(Compra compra) {
		// TODO Auto-generated method stub
		Compra comp = buscarCompra(compra.getId());
		Producto produc = productoService.buscarProducto(compra.getProducto().getCodigo());


		if (comp == null) {
			listaCompra.add(compra);
			//actualizar el stock del prodc
			 if (produc != null) {
				 produc.setStock(produc.getStock() + compra.getCantidad());
	            }

		} else {
			//si la compra ya exixte actu la su lista
			int pos = listaCompra.indexOf(comp);
			listaCompra.set(pos, compra);
			//ajut el stock del prod actualz.
			if (produc != null) {
				produc.setStock(produc.getStock() + (compra.getCantidad() - comp.getCantidad()));
            }


		}
	}

	@Override
	public Compra buscarCompra(int id) {
		// TODO Auto-generated method stub
		Compra comp = null;
		for (Compra com : listaCompra) {
			if (com.getId() == id) {
				comp = com;
				break;
			}
		}
		return comp;
	}

	@Override
	public void eliminarCompra(Compra compra) {
		// TODO Auto-generated method stub
		listaCompra.remove(compra);
		Producto producto = productoService.buscarProducto(compra.getProducto().getCodigo());
        if (producto != null) {
            producto.setStock(producto.getStock() - compra.getCantidad());
        }
	}
	
	
	

}
