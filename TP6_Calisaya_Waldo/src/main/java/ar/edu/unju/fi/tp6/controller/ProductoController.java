package ar.edu.unju.fi.tp6.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import ar.edu.unju.fi.tp6.model.Producto;
import ar.edu.unju.fi.tp6.service.IProductoService;



@Controller
public class ProductoController {
	
	@Autowired
	IProductoService serviceProducto;
	
	@GetMapping({"/" , "/index"})
	public String inicio() {
		
		return "index";
	}
	
	
	/* //cunado hay mas de un servicio
	IProductoService serviceProducto=new ServiceProductoImp(); */
	
	@GetMapping("/producto")
	public String getNewProducto(Model model) {
		Producto producto= new Producto();
		model.addAttribute("productoNuevo", producto);
		
		return "nuevoProducto";
	}
	
	@PostMapping("/producto/guardar")
	public String guardar(Producto productoNuevo) {
		serviceProducto.agregarProducto(productoNuevo);
		return "resultado";
	}
	
	@GetMapping("/producto/ultimo")
	public String getMethodName(Model model) {
		//Producto productos= serviceProducto.ultimoproducto();
		List<Producto> productos= serviceProducto.listaProductos();
		model.addAttribute("productos", productos);
		model.addAttribute("contProd" , serviceProducto.listaProductos().size());
	return "listarproducto";
	}
	
	
	@GetMapping("/edit/{codigo}")
	public String editarEmpleado(@PathVariable("codigo") int codPro, Model modelo) {
		 
		 Producto prodEditar = serviceProducto.buscarProducto(codPro);
		 
		 modelo.addAttribute("productoNuevo",prodEditar);
		 
		 return "nuevoProducto";
	 }
	
	
	//********* ELIMINAR ******************
	@GetMapping("/del/{codigo}")
	public String eliminarEmpleado(@PathVariable("codigo") int codPro, Model modelo) {
		 
		 Producto prodEliminar = serviceProducto.buscarProducto(codPro);
		 serviceProducto.eliminarProducto(prodEliminar);
		
		 
		 return "redirect:/producto/ultimo";
	}
	
	
	
}
